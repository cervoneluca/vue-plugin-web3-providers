# vue-plugin-web3-providers

A Vue plugin to simplify the handling of Web3 providers in Vue.js applications. Currently only MetaMask is supported. 

## Requirements

- [Vue.js](https://vuejs.org) (v2.6.11+)

## Plugin Install

```sh
npm install vue-plugin-web3-providers
```

## Plugin Usage

Import the plugin in your Vue app, and setup Vue to use it as the followings: 

```javascript
import web3Providers from 'vue-plugin-web3-providers';
Vue.use(web3Providers);
```

Having done this, you will have the following helpers available in all your Vue components: 

```javascript
// The provider
const provider = this.$ethereum;

// Connect the provider
provider.connect();

// ethereum info
const {
    providerConnected,
    providerName,
    chainId,
    chainName,
    userAddress,
} = this.ethereumInfo;
```
## Supported Providers

Currently the plugins support the following providers:
 - MetaMask
   - If users connect to ethereum by means of MetaMask, the `$ethereum` variable will be mapped to the MetaMask provider, so it will have all methods, events and properties defined by the [MetaMask API](https://docs.metamask.io/)

## Reactivity

The info stored in the `ethInfo` variable are all reactive. You can use them in your templates like the following:

```hmtl
<template>
    <div>
        User address: 
        <strong>{{ ethereumInfo.userAddress }}</strong>
    </div>
<template>
```

## Contribution Guide

If you want to contribute to the development of this Vue plugin, follow the [contributing guide](https://gitlab.com/cervoneluca/vue-plugin-web3-providers/-/blob/master/CONTRIBUTING.md).
