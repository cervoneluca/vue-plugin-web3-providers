const web3Provider = {
  install(Vue) {
    const vue = Vue;

    const getProvider = () => {
      if (typeof window.ethereum !== 'undefined' || (typeof window.web3 !== 'undefined')) {
        const currentProvider = window.ethereum || window.web3.currentProvider;
        return currentProvider;
      }
      return null;
    };

    const handleChainChanged = (chainId, vInst) => {
      const chain = (chainId.result) ? chainId.result.replace('0x0', '') : chainId;
      let chainName = 'mainnet';
      switch (chain) {
        case '1':
          chainName = 'mainnet';
          break;
        case '3':
          chainName = 'ropsten';
          break;
        case '42':
          chainName = 'kovan';
          break;
        case '4':
          chainName = 'rinkeby';
          break;
        case '5':
          chainName = 'goerli';
          break;
        default:
          chainName = 'private';
          break;
      }
      vue.set(vInst.ethereumInfo, 'chainId', chain);
      vue.set(vInst.ethereumInfo, 'chainName', chainName);
    };


    const handleAccountsChanged = (accounts, vInst) => {
      const newAddress = accounts[0];
      if (accounts.length === 0) {
        vue.set(vInst.ethereumInfo, 'userAddress', false);
        throw new Error('Your provider is not conneted. Please connect your provider.', 4100);
      }
      vue.set(vInst.ethereumInfo, 'userAddress', newAddress);
    };

    const connect = (provider, vInst) => {
      if (vInst.ethereumInfo.providerName === 'MetaMask') {
        provider.send('eth_requestAccounts')
        // .then(handleAccountsChanged(provider.selectedAddress, vInst))
          .catch((err) => {
            if (err.code === 4001) {
              throw new Error('User has denied access', err.code);
            } else {
              throw new Error(err);
            }
          });
      } else {
        provider.enable();
      }
    };

    vue.mixin({
      created() {
        const provider = getProvider();
        vue.prototype.$ethereum = provider;
        this.ethereumInfo.providerConnected = !!provider.selectedAddress;
        if (provider.isMetaMask) {
          this.ethereumInfo.providerName = 'MetaMask';
        }
        this.ethereumInfo.userAddress = provider.selectedAddress;
        provider.autoRefreshOnNetworkChange = false;
        provider.connect = () => connect(provider, this);
        if (provider.on) {
          provider.on('accountsChanged', (accounts) => handleAccountsChanged(accounts, this));
          provider
            .send('eth_chainId')
            .then((chainId) => handleChainChanged(chainId, this))
            .catch((err) => { throw new Error(err); });
          provider.on('networkChanged', (chainId) => handleChainChanged(chainId, this));
        }
      },
      data() {
        return {
          ethereumInfo: {
            providerConnected: false,
            providerName: 'unknown',
            chainId: null,
            chainName: null,
            userAddress: 'No accounts. Provider not connected.',
          },
        };
      },
    });
  },
};

export default web3Provider;
