# How to contribute

 - Clone this repository
 - Install dependencies
 - Write your fixes
 - Make a pull request
    
# Code style

Your code must be written according the [AirBnB](https://github.com/airbnb/javascript) code style. 
