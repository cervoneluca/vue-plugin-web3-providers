import Vue from 'vue';
import web3Provider from '@/main';
import App from './App.vue';

Vue.use(web3Provider);

new Vue({
  render: (h) => h(App),
}).$mount('#app');
